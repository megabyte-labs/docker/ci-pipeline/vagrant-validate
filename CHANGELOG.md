# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.2.16](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/vagrant/compare/v0.0.25...v2.2.16) (2021-06-14)

### [0.0.25](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/vagrant/compare/v0.0.24...v0.0.25) (2021-06-13)

### 0.0.24 (2021-06-13)

### 0.0.23 (2021-05-13)
