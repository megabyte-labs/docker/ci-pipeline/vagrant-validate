FROM ubuntu:20.04

ENV container docker
ENV VAGRANT_RELEASE_URL https://releases.hashicorp.com/vagrant/

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
RUN apt-get update -y \
  && apt-get install --no-install-recommends -y \
      ca-certificates=* \
      curl=7.* \
  && URL_REGEX="((([[:digit:]]+\.)+)[[:digit:]]+)" \
  && DEB_REGEX="[[:print:]]+64\.deb" \
  && DEST_DIR="/tmp" \
  && FILE_NAME="vagrant.deb" \
  && RELEASES=$(curl $VAGRANT_RELEASE_URL) \
  && [[ $RELEASES =~ $URL_REGEX ]] \
  && LATEST_VERSION="${BASH_REMATCH[0]}" \
  && CHECKSUMS=$(curl "${VAGRANT_RELEASE_URL%/}/${LATEST_VERSION}/vagrant_${LATEST_VERSION}_SHA256SUMS") \
  && [[ $CHECKSUMS =~ $DEB_REGEX ]] \
  && CHECKSUM_DEB="${BASH_REMATCH[0]}" \
  && CHECKSUM="${CHECKSUM_DEB%  vagrant_${LATEST_VERSION}_x86_64.deb}" \
  && curl "${VAGRANT_RELEASE_URL%/}/${LATEST_VERSION}/vagrant_${LATEST_VERSION}_x86_64.deb" -o "${DEST_DIR}/${FILE_NAME}" \
  && echo "${CHECKSUM} ${DEST_DIR}/${FILE_NAME}" | sha256sum -c \
  && apt-get install --no-install-recommends -y "${DEST_DIR}/${FILE_NAME}" \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /work
ENTRYPOINT ["vagrant"]
CMD ["--version"]

ARG BUILD_DATE
ARG REVISION
ARG VERSION

LABEL maintainer="Megabyte Labs <help@megabyte.space"
LABEL org.opencontainers.image.authors="Brian Zalewski <brian@megabyte.space>"
LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.description="Node.js files/configurations that support the creation of Dockerfiles"
LABEL org.opencontainers.image.documentation="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/vagrant/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.revision=$REVISION
LABEL org.opencontainers.image.source="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/vagrant.git"
LABEL org.opencontainers.image.url="https://megabyte.space"
LABEL org.opencontainers.image.vendor="Megabyte Labs"
LABEL org.opencontainers.image.version=$VERSION
LABEL space.megabyte.type="ci-pipeline"
